// Define a angular module
var app = angular.module('myApp', []);



//Creating Service For Fetching Data
app.service('birthdayService', function($http) {
    //Fetching Birthdays Json Data Using Promise and storing birthdayData Variable
    this.getAll = function() {

        return $http.get('birthdays.json')
            .then(function(res) {
                var data = res.data;
                data.sort(function(a, b) {
                    return new Date(a.birthday.substr(6, 4) + "/" + a.birthday.substr(3, 3) + a.birthday.substr(0, 2)).getTime() - new Date(b.birthday.substr(6, 4) + "/" + b.birthday.substr(3, 3) + b.birthday.substr(0, 2)).getTime()
                });
                return data;
            }, function myError(res) {
                console.log(res.statusText);
            });
    }
});




//Creating Controller For Wringing Logics
app.controller('BirthdayCtrl', function($scope, birthdayService) {
    birthdayService.getAll().then(function(data) {
        $scope.birthdayData = data;
        $scope.birthdayDataString = JSON.stringify($scope.birthdayData);
        console.log($scope.birthdayData);

        //Function For Creating Inner Box
        $scope.createBox = function(container, day) {
            var colNum, i, wd, ht, box, j = 0;
            colNum = Math.ceil(Math.sqrt(day.length));
            container[0].innerHTML = '';
            if (day == 0) {
                container.addClass("day-empty");
                container[0].innerHTML = '';
            } else {
                container.removeClass("day-empty");
                for (i = 0; i < day.length; i++) {
                    j++;
                    wd = 100 / colNum;
                    ht = 100 / colNum;
                    box = document.createElement('div');
                    box.innerText = day[i];
                    a = j * 0.12345678;
                    if (j == 8) {
                        j = 1;
                    }
                    box.style.backgroundColor = "#" + ((1 << 24) * a | 0).toString(16);
                    box.style.width = wd + "%";
                    box.style.height = ht + "%";
                    box.className = 'nameBox';
                    container[0].appendChild(box);
                }
            }
        };


        //Update Function When User Click on Update Function
        $scope.update = function() {
            $scope.dayArry = [];
            $scope.mon = [];
            $scope.tue = [];
            $scope.wed = [];
            $scope.thu = [];
            $scope.fri = [];
            $scope.sat = [];
            $scope.sun = [];
            $scope.userYear;
            angular.forEach($scope.birthdayData, function(value) {
                var splitDate = value.birthday.split("/");
                var day = new Date($scope.userYear + "/" + value.birthday.substr(3, 3) + value.birthday.substr(0, 2)).getDay();
                //new Date($scope.userYear, splitDate[1] - 1, splitDate[0]).getDay();
                var nameInitials = (value.name).match(/\b\w/g) || [];
                nameInitials = ((nameInitials.shift() || '') + (nameInitials.pop() || '')).toUpperCase();
                switch (day) {
                    case 0:
                        $scope.sun.push(nameInitials);
                        break;
                    case 1:
                        $scope.mon.push(nameInitials);
                        break;
                    case 2:
                        $scope.tue.push(nameInitials);
                        break;
                    case 3:
                        $scope.wed.push(nameInitials);
                        break;
                    case 4:
                        $scope.thu.push(nameInitials);
                        break;
                    case 5:
                        $scope.fri.push(nameInitials);
                        break;
                    case 6:
                        $scope.sat.push(nameInitials);
                }

            });

            //Array In which all birthday data associated with day
            $scope.dayArry = [$scope.mon, $scope.tue, $scope.wed, $scope.thu, $scope.fri, $scope.sat, $scope.sun];
            console.log($scope.dayArry);



            angular.forEach($scope.dayArry, function(day, key) {
                var container, colNum, i, wd, ht, box, j = 0;
                switch (key) {
                    case 0:
                        container = angular.element(document.getElementById('mon'));
                        $scope.createBox(container, day);
                        break;
                    case 1:
                        container = angular.element(document.getElementById('tue'));
                        $scope.createBox(container, day);
                        break;
                    case 2:
                        container = angular.element(document.getElementById('wed'));
                        $scope.createBox(container, day);
                        break;
                    case 3:
                        container = angular.element(document.getElementById('thu'));
                        $scope.createBox(container, day);
                        break;
                    case 4:
                        container = angular.element(document.getElementById('fri'));
                        $scope.createBox(container, day);
                        break;
                    case 5:
                        container = angular.element(document.getElementById('sat'));
                        $scope.createBox(container, day);
                        break;
                    case 6:
                        container = angular.element(document.getElementById('sun'));
                        $scope.createBox(container, day);
                }

            });

        };


    })


});